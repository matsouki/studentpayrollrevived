﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentPayrollRevived.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DepartmentId { get; set; }
        public string BillableAccount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? AllottedHours { get; set; }
        public int? HoursUsed { get; set; }
        public bool SupervisorCreated { get; set; }
        public bool ElevatedPay { get; set; }
        public bool Active { get; set; }
        public bool PayRate { get; set; }
        public DepartmentModel Department { get; set; }
    }
}
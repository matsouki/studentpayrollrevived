﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentPayrollRevived.Models
{
    public class DepartmentEmployeeModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int DepartmentId { get; set; }
        public virtual EmployeeModel Employee { get; set; }
        public virtual DepartmentModel Department { get; set; }
    }
}
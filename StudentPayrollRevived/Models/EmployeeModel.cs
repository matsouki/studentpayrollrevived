﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentPayrollRevived.Models
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public string PanId { get; set; }
        public string NetId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName {
            get { return string.Format("{0} {1}", FirstName, LastName); }
            set { }
        }
        public string Position { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double StartingPay { get; set; }
        public double CurrentPay { get; set; }
        public int UserTypeId { get; set; }
        public string Assistanceship { get; set; }
        public bool Active { get; set; }
        public UserTypeModel UserType { get; set; }
        public List<EmployeeTimesModel> EmployeeTimes { get; set; } = new List<EmployeeTimesModel>();
    }
}
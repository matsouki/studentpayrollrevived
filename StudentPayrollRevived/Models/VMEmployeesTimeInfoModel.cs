﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentPayrollRevived.Interfaces;
namespace StudentPayrollRevived.Models
{
    public class VMEmployeesTimeInfoModel : IViewModel
    {
        public SignedInUserModel SignedInUser { get; set; }
        public PayPeriodModel PayPeriod { get; set; }
        public bool IsSupervisor { get; set; } = false;
        public List<DepartmentEmployeeModel> DepartmentEmployees { get; set; } = new List<DepartmentEmployeeModel>();
    }
}
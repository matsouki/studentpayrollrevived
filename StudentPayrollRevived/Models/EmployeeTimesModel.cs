﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentPayrollRevived.Models
{
    public class EmployeeTimesModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public int HoursWorked { get; set; }
        public int MinutesWorked { get; set; }
        public string Comment { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public EmployeeModel Employee { get; set; }
        public ProjectModel Project { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentPayrollRevived.Models
{
    public class PayPeriodModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
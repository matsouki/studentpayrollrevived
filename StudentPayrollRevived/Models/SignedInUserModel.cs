﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentPayrollRevived.Models
{
    public class SignedInUserModel
    {
        public string NetID { get; set; } = "--NETID NOT SET--";
        public string DisplayName { get; set; } = "--USERNAME--";
        public List<string> Roles { get; set; } = new List<string>();
        public List<DepartmentModel> Departments { get; set; } = new List<DepartmentModel>();
    }
}
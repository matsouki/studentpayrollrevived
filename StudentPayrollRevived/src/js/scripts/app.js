// React---------------------------------------
import React from 'react';
import ReactDOM from 'react-dom';
import Loadable from "react-loadable";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'; 
// Other---------------------------------------
import baseUrl from "../../js/baseUrl";
// Store---------------------------------------
import configureStore from "../store/configureStore";
const store = configureStore();
// Loadable Components------------------------
const LoadableNavbarHOCLayout = Loadable({
    loader: () => import("../components/layout/Navbar/NavbarHOCLayout"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
const LoadableFooter = Loadable({
    loader: () => import("../components/layout/Footer"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
// Loadable Pages-----------------------------
const LoadableIndex = Loadable({
    loader: () => import("../Pages/Index"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});


const routes = (
    <Router>
        <Provider store={store}>
            <div>
                <LoadableNavbarHOCLayout />
                <Switch>
                    <Route path={`${baseUrl.route}`} component={() => <LoadableIndex />} exact={true} />
                </Switch> 
                <LoadableFooter />
            </div>
        </Provider>
    </Router>
);

var appRoot = document.getElementById("root");
ReactDOM.render(routes, appRoot);
var nojs = document.getElementById("nojs");
document.body.removeChild(nojs);
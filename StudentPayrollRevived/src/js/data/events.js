export default [
    {
        Id: 1,
        key: "1",
        Active: true,
        Link: "#",
        Text: "Introduction to ArcGIS for MDOT",
        Date: new Date("JAN 14 2019")
    },
    {
        Id: 2,
        key: "2",
        Active: false,
        Link: "#",
        Text: "ArcGIS II for MDOT",
        Date: new Date("JAN 15 2019")
    },
    {
        Id: 3,
        key: "3",
        Active: false,
        Link: "#",
        Text: "Introduction to ArcGIS",
        Date: new Date("MAR 4 2019")
    },
    {
        Id: 4,
        key: "4",
        Active: false,
        Link: "#",
        Text: "ArcGIS II - Beyond the Basics",
        Date: new Date("MAR 6 2019")
    },   
    {
        Id: 5,
        key: "5",
        Active: false,
        Link: "#",
        Text: "Introduction to ArcGIS Online and Collector for ArcGIS",
        Date: new Date("MAR 7 2019")
    },   
]
export default [
    {
        Id: 1,
        key: "1",
        Active: true,
        Link: "#",
        ImgLink: "/images/featurednews1.jpg",
        src: "images/featurednews1.jpg",
        webp: "images/featurednews1.webp",
        Text: "2019 Course Schedule Added",
        Date: new Date("NOV 15 2018")
    }
]
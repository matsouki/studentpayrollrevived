const defaultState = {
    showMobileMenu: false
}

export default (state = defaultState, action) => {
    if (action.type === "TOGGLE_MOBILE_MENU") {
        return {
            ...state,
            showMobileMenu: !state.showMobileMenu
        }
    }
    else {
        return state;
    }
};


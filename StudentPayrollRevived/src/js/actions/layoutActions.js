/**
 * This action is going to send a view to the reducer to update the layout
 * @param {string} view 
 */
export const setLayoutType = (view="standard") => ({
    type: "SET_LAYOUT_TYPE",
    view
});
export const togglePowerMenu = () => ({
    type: "TOGGLE_POWERMENU",
});
export const setPowerMenuItems = (view=null) => ({
    type: "SET_POWER_MENU_ITEMS",
    view
});
export const setPowerMenu = (show) => ({
    type: "SET_POWER_MENU",
    show
});
export const toggleMegaMenu = () => ({
    type: "TOGGLE_MEGAMENU",
});
export const setMegaMenuItems = (view=null) => ({
    type: "SET_MEGA_MENU_ITEMS",
    view
});
export const setMegaMenu = (show) => ({
    type: "SET_MEGA_MENU",
    show
});
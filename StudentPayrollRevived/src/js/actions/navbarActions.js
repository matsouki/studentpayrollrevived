/**
 * This action is going to send a view to the reducer to update the layout
 * @param {string} view 
 */
export const toggleMobileMenu = () => ({
    type: "TOGGLE_MOBILE_MENU"
});
/**
 * This action is going to send a view to the reducer to update the layout
 * @param {string} view 
 */
export const AddEventsItem = (obj={}) => ({
    type: "ADD_EVENTS_ITEM",
    obj
});
export const SetEventsItemActive = (id=1) => ({
    type: "SET_ITEM_ACTIVE",
    id
});

export const ClearEvents = () => ({
    type: "CLEAR_EVENTS"
});

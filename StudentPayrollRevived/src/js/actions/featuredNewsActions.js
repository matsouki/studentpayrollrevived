/**
 * This action is going to send a view to the reducer to update the layout
 * @param {string} view 
 */
export const AddNewsItem = (obj={}) => ({
    type: "ADD_NEWS_ITEM",
    obj
});
export const SetNewsItemActive = (id=1) => ({
    type: "SET_ITEM_ACTIVE",
    id
});

export const ClearNews = () => ({
    type: "CLEAR_NEWS"
});

// React---------------------------------------
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

/* IMPORTANT: THIS IS OLD AND COULD BE DONE IN A BETTER WAY */

/*
    All posible this.props
        title       - string
        items       - array of objects
        preUrl      - string: need if items exist
        goBackUrl   - string
*/

class RightNav extends Component{
	constructor(props){
		super(props);
		this.state =  {
		};
    }
    
    render(){
        return(
            <div id="SmallPane1" className="content col-12 col-md-3">
                
                <div id="RightNav" className="rightNav">
                    <div className="__line"></div>
                    
                    {/* Gets title from this.props, if not there then display nothing */}
                    {this.props.title ?
                        <ul className="__list">
                            <li className="__item small">
                                <h2>{this.props.title}</h2>
                            </li>
                        </ul>
                    :
                        ""
                    }

                    {/* Gets items to iterate over from this.props, if not there then display nothing */}
                    {this.props.items && this.props.items.length > 0 ?
                        <ul className="__list small">
                            {this.props.items.map((item) => (
                                <li key={item.Id} className="__item">
                                    {/* Gets preUrl from this.props, if not there then display console log error message */}
                                    {this.props.preUrl ? 
                                        <Link to={`${this.props.preUrl}/${item.Code}`}>
                                            {item.Display}
                                        </Link>
                                    :
                                        console.log("ERROR: Please make sure the prop 'preUrl' is getting passed to the RightNav component!")
                                    }
                                </li>
                            ))}
                        </ul>
                    :
                        ""
                    }

                    {/* Gets url to go back to from this.props, if not there then display nothing */}
                    {this.props.goBackUrl ?
                        <ul className="__list small">
                            <li className="__item">
                                <Link to={this.props.goBackUrl}>
                                    Go Back
                                </Link>
                            </li>
                        </ul>
                    :
                        ""
                    }
                </div>

                {/* This is where dashboard goes */}
            </div>
        );
    }
}
export default RightNav;
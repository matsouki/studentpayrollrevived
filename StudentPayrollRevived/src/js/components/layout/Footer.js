// React---------------------------------------
import React from 'react';
import { Link } from 'react-router-dom';
// Other---------------------------------------
import baseUrl from "../../baseUrl";

const Footer = () => (
    <div className="wrapper __wrapper">
        <div className="bottom-links rsgis-dynamic-header rsgis-left-bordered-list cust-footer">
            <div className="container">
                <div className="row">
                    <div id="BottomOuterLeftPane" className="content col-12 col-md-4">
                        <div className="__header __header--left __header--small __header--green __header--normal-type ">
                            <div className="__text" role="heading">Services</div>
                        </div>
                        {/* End Dynamic Header */}
                        <ul className="rsgis-list-left-border">
                            <li><Link to="#" aria-label="GIS">GIS</Link></li>
                            <li><Link to="#" aria-label="Imagery">Imagery</Link></li>
                            <li><Link to="#" aria-label="Application Development">Application Development</Link></li>
                            <li><Link to="#" aria-label="GPS">GPS</Link></li>
                            <li><Link to="#" aria-label="Cartography + Graphic Design">Cartography + Graphic Design</Link></li>
                            <li><Link to="#" aria-label="Web Design">Web Design</Link></li>
                            <li><Link to="#" aria-label="Training">Training</Link></li>
                        </ul>
                        {/* End Left List */}
                    </div>
                    {/* End BottomOuterLeftPane */}
                    <div id="BottomInnerLeftPane" className="content col-12 col-md-4">
                        <div className="__header __header--left __header--small __header--green __header--normal-type ">
                            <div className="__text" role="heading">Education</div>
                        </div>
                        {/* End Dynamic Header */}
                        <ul className="rsgis-list-left-border">
                            <li><Link to="#" aria-label="Instructor-Led Training">Instructor-Led Training</Link></li>
                            <li><Link to="#" aria-label="Online Training">Online Training</Link></li>
                            <li><Link to="#" aria-label="Documents">Documents</Link></li>
                            <li><Link to="#" aria-label="Our Classroom">Our Classroom</Link></li>
                            <li><Link to="#" aria-label="Workshop Descriptions">Workshop Descriptions</Link></li>
                        </ul>
                        {/* End Left List */}
                    </div>
                    {/* End BottomInnerLeftPane */}
                    <div id="BottomInnerRightPane" className="content col-12 col-md-4">
                        <div className="__header __header--left __header--small __header--green __header--normal-type ">
                            <div className="__text" role="heading">About Us</div>
                        </div>
                        {/* End Dynamic Header */}
                        <ul className="rsgis-list-left-border">
                            <li><Link to="#" aria-label="Location &amp; Directions">Location &amp; Directions</Link></li>
                            <li><Link to="#" aria-label="Mission">Mission</Link></li>
                            <li><Link to="#" aria-label="Staff">Staff</Link></li>
                            <li><Link to="#" aria-label="History">History</Link></li>
                            <li><Link to="#" aria-label="Hall of Fame">Hall of Fame</Link></li>
                        </ul>
                        {/* End Left List */}
                    </div>
                    {/* End BottomInnerRightPane */}
                </div>
            </div>
        </div>
        
        <div className="bottom-social cust-bottom-social">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <ul className="social-list">
                            <li>
                                <a className="link link--facebook" href="http://facebook.com" target="_blank" aria-label="Facebook">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a className="link link--snap" href="http://linkedin.com" target="_blank" aria-label="Instagram">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a className="link link--snap" href="http://snapchat.com" target="_blank" aria-label="Snapchat">
                                    <i class="fab fa-snapchat-ghost"></i>
                                </a>
                            </li>
                            <li>
                                <a className="link link--twitter" href="http://twitter.com" target="_blank" aria-label="Twitter">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                        </ul>
                        {/* End Social Icons */}
                    </div>
                </div>
            </div>
        </div>
        {/* End Social Icons */}
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <footer>
                        <div className="row">
                            <div className="col-12 col-md-4 mobile-hide">
                                <a href="https://msu.edu/" target="_blank">
                                    <img   
                                        alt="Michigan State University wordmark" 
                                        className="screen-msuwordmark" 
                                        src={`${baseUrl.path}/images/msu-wordmark-green.svg`} 
                                    />
                                </a>
                            </div>
                            <div className="col-12 col-md-8">
                                <div className="row">
                                    <div className="col-12">
                                        <ul className="modify-desktop cust-footer-info-list">
                                            <li className="bold">
                                                <a href="tel:5173557505" target="_blank">(517) 355-7505</a>
                                            </li>
                                            <li>
                                                <Link to="/about">
                                                    Contact Information
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="#">
                                                    Site Map
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="#">
                                                    Privacy Statement
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="#">
                                                    Site Accessibility
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-md-12">
                                        <ul className="tiny border-top">
                                            <li>Call MSU:
                                                <span className="bold">(517) 355-1855</span>
                                            </li>
                                            <li>Visit:
                                                <a className="highlight" href="https://msu.edu/" target="_blank">msu.edu</a>
                                            </li>
                                            <li>MSU is an affirmative-action, equal-opportunity employer.</li>
                                            <li>
                                                <a className="highlight" href="http://oie.msu.edu/" target="_blank">Notice of Nondiscrimination</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-md-12">
                                        <ul className="tiny no-margin">
                                            <li>
                                                <span className="bold">Spartans Will</span>
                                            </li>
                                            <li>&copy; Michigan State University</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mobile-show">
                            <div className="col-12 col-md-3">
                                <a href="https://msu.edu/" target="_blank">
                                    <img 
                                        alt="Michigan State University wordmark" 
                                        className="screen-msuwordmark" 
                                        src={`${baseUrl.path}/images/msu-wordmark-green.svg`}
                                    />
                                </a>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
);
export default Footer;
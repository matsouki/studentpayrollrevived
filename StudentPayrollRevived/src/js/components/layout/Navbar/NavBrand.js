// React---------------------------------------
import React, { Component } from "react";
import { Link } from "react-router-dom";

class NavBrand extends Component {
    constructor(props) {
        super(props)
    }
    GetClassName() {
        if (this.props.show == "true") {
            return "__main-header __main-header--visible"
        } else {
            return "__main-header __main-header--hidden"
        }
    }
    render() {
        return <div className={this.props.layoutView === "standard" ? "__header __header--standard" : "__header"}>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h1 className={this.GetClassName()}>
                            <a tabIndex="4" href="#" className="rsgis_logo" aria-label="RS&amp;GIS">RS<span>&amp;</span>GIS</a>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    }
}
export default NavBrand;
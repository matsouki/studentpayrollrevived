// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
// Actions-------------------------------------
import {toggleMobileMenu} from "../../../actions/navbarActions"

class NavHamburger extends Component {
    constructor(props) {
        super(props);
        this.toggleMenu = this.toggleMenu.bind(this);
    }
    getClassName(){
        let classname = "__hamburger __hamburger--transparent ";
        if(this.props.layout.view === "standard"){
            classname = "__hamburger __hamburger "
        }
        if(this.props.navbar.showMobileMenu){
            classname += "__hamburger--close __hamburger--finished";
        }
        return classname;
    }
    toggleMenu(e){
        console.log("toggle mobile menu");
        this.props.actions.toggleMobileMenu();
    }
    render() {
        return <div id="MobileHamburger" className="col-3 hamburger">
            <button
                aria-label="Toggle Mobile Navagation Menu"
                className={this.getClassName()}
                onClick={(e) => this.toggleMenu(e)}>
                <div className="__line"></div>
                <div className="__line"></div>
                <div className="__line"></div>
            </button>
        </div>
    }
}




const mapStateToProps = (state) => {
    return {
        layout: state.layout,
        navbar: state.navbar
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            toggleMobileMenu: () => { dispatch(toggleMobileMenu()) },
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(NavHamburger);

// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loadable from "react-loadable";
import { withRouter } from 'react-router-dom';
// Loadable Components-------------------------
import NavBrand from "./NavBrand"
import NavBarNav from "./NavBarNav"
// const LoadableNavBrand = Loadable({
//     loader: () => import("./NavBrand"),
//     loading() {
//         return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
//     }
// });
// const LoadableNavbarNav = Loadable({
//     loader: () => import("./NavBarNav"),
//     loading() {
//         return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
//     }
// });

const LoadableMegaMenu = Loadable({
    loader: () => import("./MegaMenu/MegaMenu"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});


class NavBarMain extends Component {
    constructor(props) {
        super(props);
    }
    ShowBrand() {
        if (this.props.navbar.showMobileMenu) {
            return <NavBrand show="false" layoutView={this.props.layout.view} />
        } else {
            return <NavBrand show="true" layoutView={this.props.layout.view} />
        }
    }
    render() {
        return <div className={this.props.layout.view === "standard" ? "NavBarContainer NavBarContainer--relative" : "NavBarContainer"}>
            {this.ShowBrand()}
            <NavBarNav layoutView={this.props.layout.view} />
            <LoadableMegaMenu />
        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        layout: state.layout,
        navbar: state.navbar
    };
};
export default withRouter(connect(mapStateToProps)(NavBarMain));
// React---------------------------------------
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Loadable from "react-loadable";
// Actions-------------------------------------
import { setLayoutType } from '../../../actions/layoutActions';
// Loading Conponents--------------------------
import SkeletonSearchBarContainer from "../../Skeletons/SearchBarContainer";
// Loadable Components-------------------------
const LoadableTransparentNavbar = Loadable({
    loader: () => import("./TransparentNavbar"),
    loading() {
        return <SkeletonSearchBarContainer/>
    }
});
const LoadableStandardNavbar = Loadable({
    loader: () => import("./StardardNavbar"),
    loading() {
        return <SkeletonSearchBarContainer/>
    }
});



class NavbarHOCLayout extends Component {
    constructor(props) {
        super(props);
        this.toggleNavbarType = this.toggleNavbarType.bind(this);
    }
    GetLayoutNav(){
        if(this.props.layout.view === "standard"){
            return <LoadableStandardNavbar />;
        }else{
            return <LoadableTransparentNavbar />;
        }
    }
    toggleNavbarType(e){
        if(this.props.layout.view === "standard"){
            this.props.actions.setLayoutType("transparent");
        }else{
            this.props.actions.setLayoutType("standard");
        }
    }
    render() {
        return (
            <div>
               {this.GetLayoutNav()} 
               {/* <button onClick={(e)=>this.toggleNavbarType(e)} className="nav_test_btn btn btn-info" aria-label="Toggle Nav Type" >Toggle Nav Type</button> */}
            </div>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        layout: state.layout
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavbarHOCLayout));
// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loadable from "react-loadable";

import { withRouter } from 'react-router-dom';
// Other---------------------------------------
import baseUrl from "../../../baseUrl";
// Loading Components-------------------------
import SkeletonNavBar from "../../Skeletons/NavBar";
// Loadable Components-------------------------
const LoadableSearchInput = Loadable({
    loader: () => import("./NavSearch"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
const LoadableNavHamburger = Loadable({
    loader: () => import("./NavHamburger"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
const LoadableNavBarMain = Loadable({
    loader: () => import("./NavBarMain"),
    loading() {
        return <SkeletonNavBar/>
    }
});

class StandardNavbar extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    GetClassName() {
        if (this.props.navbar.showMobileMenu) {
            return "SearchBarContainer SearchBarContainer--nudged";
        } else {
            return "SearchBarContainer";
        }
    }
    GetHeaderClassName() {
        if (this.state.top) {
            return "__header __header--standard";
        }
        else if (this.state.scrolling && this.state.direction == 1) {
            return "__header __header--hidden";
        } else {
            return "__header";
        }
    }
    
    render() {
        return <div>
            <div className={this.GetClassName()}>
                <div className="__header">
                    <div className="container">
                        <div className="row logo-row">
                        <div id="MSULOGO" className="col">
                            <a tabIndex="1" href="https://msu.edu" target="_blank">
                                <img
                                    alt="Michigan State University"
                                    title="Michigan State University Logo Green"
                                    id="main_logo"
                                    className="__logo"
                                    src={`${baseUrl.path}/images/masthead-helmet-green.svg`}
                                />
                            </a>
                        </div>
                            <LoadableNavHamburger />
                            <LoadableSearchInput />
                        </div>
                    </div>
                </div>
            </div>
            <LoadableNavBarMain />
        </div>
    }
}


const mapStateToProps = (state) => {
    return {
        layout: state.layout,
        navbar: state.navbar
    };
};

export default withRouter(connect(mapStateToProps)(StandardNavbar));

// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
// Other---------------------------------------
import baseUrl from "../../../../baseUrl";


class AboutMegaMenu extends Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
    }
    render() {
        return (
            <ul className="__mm-list">
                <li><Link to={`${baseUrl.route}`}>Location &amp; Directions</Link></li>
                <li><Link to={`${baseUrl.route}`}>Mission</Link></li>
                <li><Link to={`${baseUrl.route}`}>Staff</Link></li>
                <li><Link to={`${baseUrl.route}`}>History</Link></li>
                <li><Link to={`${baseUrl.route}`}>Hall of Fame</Link></li>
            </ul>
        )
    }
}
const mapStateToProps = (state) => {
    return {
    };
};
export default connect(mapStateToProps)(AboutMegaMenu);
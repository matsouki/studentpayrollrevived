// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
// Other---------------------------------------
import baseUrl from "../../../../baseUrl";


class ServicesMegaMenu extends Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
    }
    render() {
        return (
            <div className="row">
                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <ul className="__mm-list">
                        <li><Link to={`${baseUrl.route}services/gis`}>GIS</Link></li>
                        <li><Link to={`${baseUrl.route}services/uas`}>UAS</Link></li>
                        <li><Link to={`${baseUrl.route}services/imagery`}>Imagery</Link></li>
                        <li><Link to={`${baseUrl.route}services/applicationdevelopment`}>Application Development</Link></li>
                    </ul>
                </div>
                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <ul className="__mm-list">
                        <li><Link to={`${baseUrl.route}services/gps`}>GPS</Link></li>
                        <li><Link to={`${baseUrl.route}services/cartographygraphicdesign`}>Cartography & Graphic Design</Link></li>
                        <li><Link to={`${baseUrl.route}services/webdesign`}>Web Design</Link></li>
                        <li><Link to={`${baseUrl.route}services/training`}>Training</Link></li>
                    </ul>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
    };
};
export default connect(mapStateToProps)(ServicesMegaMenu);
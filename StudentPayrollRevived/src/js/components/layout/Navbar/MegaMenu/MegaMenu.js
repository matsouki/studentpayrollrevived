// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loadable from "react-loadable";
import { withRouter } from 'react-router-dom';
// Actions-------------------------------------
import { setMegaMenu } from "../../../../actions/layoutActions";
// Other---------------------------------------
import MegaMenuLoading from "./MegaMenuLoading";
// Loadable Components-------------------------
const LoadableAboutMegaMenu = Loadable({
    loader: () => import("./AboutMegaMenu"),
    loading() {
        return <MegaMenuLoading />
    }
});

class MegaMenu extends Component {
    constructor(props) {
        super(props);
        this.getItems = this.getItems.bind(this);
    }
    componentDidMount() {

    }

    getClassName() {
        if (this.props.navbar.showMobileMenu) {
            return "__MegaMenu __MegaMenu--showMobileMenu"
        } else {
            return "__MegaMenu"
        }
    }

    getItems() {
        let MegaMenuItems = this.props.layout.megaMenuItems;
        // console.log(this.props.layout.megaMenuItems)
        if (MegaMenuItems !== null) {
            if(MegaMenuItems==="about"){
                return <LoadableAboutMegaMenu />
            }
        } else {
            return null;
        }
    }
    exitMegaMenu() {
        this.props.actions.setMegaMenu(null);
    }
    render() {
        if (this.props.layout.showMegaMenu) {
            return (
                <div 
                    // className={this.props.layout.megaMenuItems==="auth" ? "__MegaMenu __mm-small" : "__MegaMenu"} 
                    className={this.getClassName()}
                    onClick={() => { this.exitMegaMenu() }} 
                    onMouseLeave={() => { this.exitMegaMenu() }}
                >
                    <div className="container" >
                        {this.getItems()}
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }
}
const mapStateToProps = (state) => {
    return {
        layout: state.layout,
        navbar: state.navbar        
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setMegaMenu: (show = false) => { dispatch(setMegaMenu(show)) }
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MegaMenu));
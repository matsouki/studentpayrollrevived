import React from 'react';
export default (props) => (
    <div className="row">
        <div className="col">
            <div className="skeleton skeleton--left skeleton--no-border">
                <div className="__header"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__image"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
        </div>
        <div className="col">
            <div className="skeleton skeleton--left skeleton--no-border">
                <div className="__header"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__image"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
        </div>
        <div className="col">
            <div className="skeleton skeleton--left skeleton--no-border">
                <div className="__header"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__image"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
            <div className="skeleton skeleton--no-border">
                <div className="__text_block"></div>
            </div>
        </div>
    </div>
);
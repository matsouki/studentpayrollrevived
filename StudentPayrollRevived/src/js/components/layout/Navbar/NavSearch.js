import React, { Component } from 'react';

class NavSearch extends Component {
    render() {
        return <div className="col __desktop">
            <div className="input-group mb-3">
                <input tabIndex="2" type="text" className="form-control" placeholder="Search..." aria-label="Site Search" aria-describedby="Site search" />
                <div className="input-group-append">
                    <button tabIndex="3" className="btn btn-secondary" type="button" aria-label="Search RSGIS" ><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>
    }
}

export default NavSearch;
// React---------------------------------------
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Loadable from "react-loadable";
// Other---------------------------------------
import baseUrl from "../../../baseUrl";
import {setMegaMenuItems, toggleMegaMenu, setMegaMenu} from "../../../actions/layoutActions";
// Loadable Components-------------------------
const LoadableSearchInput = Loadable({
    loader: () => import("./NavSearch"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});

class NavBarNav extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    GetClassName() {
        if (this.props.navbar.showMobileMenu) {
            return "__bar __bar--showMobileMenu"
        }
        return "__bar";
    }
    toggleMegaMenu(view){
        this.props.actions.toggleMegaMenu();
        this.props.actions.setMegaMenuItems(view);                     
    }
    setMegaMenuItems(view){
        this.props.actions.setMegaMenuItems(view);
    }
    setMegaMenu(show, view = null){
        this.props.actions.setMegaMenu(show);
        this.props.actions.setMegaMenuItems(view);
    }

    showMegaMenu(e, show, view = null){
        e.preventDefault();
        this.props.actions.toggleMegaMenu(); 
        this.props.actions.setMegaMenu(show);
        this.props.actions.setMegaMenuItems(view); 
    }

    render() {
        return (
            <div className={this.GetClassName()}>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="__mobile_nav_content">
                                <h1 className="__main-header">
                                    <Link tabIndex="4" to={`${baseUrl.route}`} className="rsgis_logo">RS<span>&amp;</span>GIS</Link>
                                </h1>
                                <LoadableSearchInput />
                            </div>
                            <ul className="__list">
                                <li className="__item" onTouchStart={(e)=>this.setMegaMenu(false)} onMouseEnter={(e)=>this.setMegaMenu(false)} onFocus={(e)=>this.setMegaMenu(false)}>
                                    <Link tabIndex="5" to={`${baseUrl.route}`} onMouseEnter={(e)=>this.setMegaMenu(false)} onFocus={(e)=>this.setMegaMenu(false)}>Home</Link>
                                </li>
                                <li className="__item">
                                    {/* <Link tabIndex="6" to={`${baseUrl.route}/stations`} onMouseEnter={(e)=>this.setMegaMenu(false)} onFocus={(e)=>this.setMegaMenu(false)}>Maps</Link>*/}
                                    <a href="#"
                                        tabIndex="6"
                                        onFocus={(e)=>this.showMegaMenu(e, true, 'services')}  
                                        onClick={(e) => this.showMegaMenu(e, true, 'services')} 
                                        onMouseEnter={(e)=>this.showMegaMenu(e, true, 'services')}
                                    >
                                        Services
                                    </a>
                                </li>
                                <li className="__item">
                                    <a href="#"
                                        tabIndex="7"
                                        onFocus={(e)=>this.showMegaMenu(e, true, 'education')}  
                                        onClick={(e) => this.showMegaMenu(e, true, 'education')} 
                                        onMouseEnter={(e)=>this.showMegaMenu(e, true, 'education')}
                                    >
                                        Education
                                    </a>
                                </li>
                                <li className="__item">
                                    <a href="#"
                                        tabIndex="8"
                                        onFocus={(e)=>this.showMegaMenu(e, true, 'archive')}  
                                        onClick={(e) => this.showMegaMenu(e, true, 'archive')} 
                                        onMouseEnter={(e)=>this.showMegaMenu(e, true, 'archive')}
                                    >
                                        Aerial Imagery Archive
                                    </a>
                                </li>
                                <li className="__item">
                                    <a href="#"
                                        tabIndex="9"
                                        onFocus={(e)=>this.showMegaMenu(e, true, 'about')}  
                                        onClick={(e) => this.showMegaMenu(e, true, 'about')} 
                                        onMouseEnter={(e)=>this.showMegaMenu(e, true, 'about')}
                                    >
                                        About Us
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}




const mapStateToProps = (state) => {
    return {
        layout: state.layout,
        navbar: state.navbar
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setMegaMenuItems: (str = null) => { dispatch(setMegaMenuItems(str)) },
            toggleMegaMenu: () => { dispatch(toggleMegaMenu()) },
            setMegaMenu: (show = false) => {dispatch(setMegaMenu(show))}
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBarNav);
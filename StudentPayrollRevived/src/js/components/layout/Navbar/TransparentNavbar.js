// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loadable from "react-loadable";
import { withRouter } from 'react-router-dom';
// Other---------------------------------------
import baseUrl from "../../../baseUrl";
// Loading Components-------------------------
import SkeletonNavBar from "../../Skeletons/NavBar";
// Loadable Components-------------------------
const LoadableSearchInput = Loadable({
    loader: () => import("./NavSearch"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
const LoadableNavHamburger = Loadable({
    loader: () => import("./NavHamburger"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
const LoadableNavBarMain = Loadable({
    loader: () => import("./NavBarMain"),
    loading() {
        return <SkeletonNavBar/>
    }
});



class TransparentNavbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scrolling: false,
            top: true,
            lastScrollY: 0,
            direction: 1 // down
        }
        this.handleScroll = this.handleScroll.bind(this);
    }
    GetClassName() {
        if (this.props.navbar.showMobileMenu) {
            if (this.state.top) {
                return "SearchBarContainer SearchBarContainer--gradient SearchBarContainer--nudged";
            } else {
                return "SearchBarContainer SearchBarContainer--nudged";
            }
        } else {
            if (this.state.top) {
                return "SearchBarContainer SearchBarContainer--gradient ";
            } else {
                return "SearchBarContainer";
            }

        }
    }
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll(event) {
        if (window.scrollY === 0 && this.state.scrolling === true) {
            this.setState((state) => {
                let direction = 1;
                if (state.lastScrollY > window.scrollY) {
                    direction = 0;
                }
                let newState = {
                    scrolling: false,
                    top: true,
                    lastScrollY: window.scrollY,
                    direction
                };
                return newState;
            });
        }
        else if (window.scrollY !== 0) {
            this.setState((state) => {
                let direction = 1;
                if (state.lastScrollY > window.scrollY) {
                    direction = 0;
                }
                let newState = {
                    scrolling: true,
                    top: false,
                    lastScrollY: window.scrollY,
                    direction
                };
                return newState;
            });
        }
    }
    GetHeaderClassName() {
        if (this.state.top) {
            return "__header __header--transparent";
        }
        else if (this.state.scrolling && this.state.direction == 1) {
            return "__header __header--hidden";
        } else {
            return "__header";
        }
    }
    GetLogoImage() {
        if (this.state.top) {
            return <a href="https://msu.edu" target="_blank">
                <img
                    alt="Michigan State University"
                    title="Michigan State University Logo White"
                    className="__logo"
                    id="transparent_logo"
                    src={`${baseUrl.path}/images/masthead-helmet-white.svg`}
                />
            </a>
        } else {
            return <a href="https://msu.edu" target="_blank">
                <img
                    alt="Michigan State University"
                    title="Michigan State University Logo Green"
                    id="main_logo"
                    className="__logo"
                    src={`${baseUrl.path}/images/masthead-helmet-green.svg`}
                />
            </a>;
        }
    }
    render() {
        return <div>
            <div className={this.GetClassName()}>
                <div className={this.GetHeaderClassName()}>
                    <div className="container">
                    <div className="row logo-row">
                            <div id="MSULOGO" className="col">
                                {this.GetLogoImage()}
                            </div>
                            <LoadableNavHamburger />
                            <LoadableSearchInput />
                        </div>
                    </div>
                </div>
            </div>
            <LoadableNavBarMain />
        </div>
    }
}
const mapStateToProps = (state) => {
    return {
        layout: state.layout,
        navbar: state.navbar
    };
};
export default withRouter(connect(mapStateToProps)(TransparentNavbar));

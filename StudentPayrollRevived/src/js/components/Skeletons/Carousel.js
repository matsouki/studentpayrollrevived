import React from 'react';
export default (props) => (
    <div className="skeleton skeleton--no-border">
        <div className="__carousel-image"></div>
    </div>
);
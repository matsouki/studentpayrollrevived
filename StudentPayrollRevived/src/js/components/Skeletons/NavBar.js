import React from 'react';
export default (props) => (
    <div className="NavBarContainer">
        <div className="__header">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h1 className="__main-header __main-header--visible">
                            <a href="#" aria-label="Enviroweather">
                                Enviroweather
                            </a>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div className="__bar">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="__mobile_nav_content">
                            <h1 className="__main-header"><a tabIndex="4" href="#">Enviroweather</a></h1>
                        </div>
                        <ul className="__list">
                            <li className="__item"><a tabIndex="5" href="#">Home</a></li>
                            <li className="__item"><a tabIndex="6" href="#">Maps</a></li>
                            <li className="__item"><a tabIndex="8" href="#">Crops</a></li>
                            <li className="__item"><a tabIndex="10" href="#">About</a></li>
                            <li className="__item"><a tabIndex="11" href="#">SDL</a></li>
                        </ul>
                    </div>
                    <div className="col-sm-4 col-lg-2">
                        <ul className="__list __list--right">
                            <li className="__item"><a href="#" tabIndex="12"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
import React from 'react';
export default (props) => (
    <div className="SearchBarContainer SearchBarContainer--gradient ">
        <div className="__header __header--transparent">
            <div className="container">
                <div className="row logo-row">
                    <div id="MSULOGO" className="col">
                        <a href="https://msu.edu" target="_blank">
                            <img
                                alt="Michigan State University" title="Michigan State University Logo White" className="__logo"
                                id="transparent_logo" src="/public/images/masthead-helmet-white.svg"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
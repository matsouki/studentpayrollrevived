// React---------------------------------------
import React, { Component } from 'react';
import baseUrl from "../../baseUrl";
// this will be replaced by a store 

class FeaturedNewsImage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const {src, webp, Text} = this.props.Item;
        return (
            <picture>
                <source className="__image" srcSet={baseUrl.path + webp} type="image/webp" alt={Text}/>
                <source className="__image" srcSet={baseUrl.path + src} type="image/jpeg"  alt={Text}/>
                <img className="__image" alt={Text} src={baseUrl.path + src} />
            </picture>
        );
    }
}


export default FeaturedNewsImage;

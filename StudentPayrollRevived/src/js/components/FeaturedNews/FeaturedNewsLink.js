// React---------------------------------------
import React, { Component } from 'react';
import { Link as ALink } from 'react-router-dom';
// this will be replaced by a store 

class FeaturedNewsLink extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const {Link, Text} = this.props.Item;
        return (
            <h4 className="__link">
                <ALink to={Link} alt="You will be redirected to a featured news item" title={Text}>{Text}</ALink>
            </h4>
        );
    }
}


export default FeaturedNewsLink;

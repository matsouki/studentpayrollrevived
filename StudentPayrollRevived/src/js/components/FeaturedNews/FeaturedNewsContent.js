// React---------------------------------------
import React, { Component } from 'react';
import FeaturedNewsDate from "./FeaturedNewsDate";
import FeaturedNewsLink from "./FeaturedNewsLink";
import FeaturedNewsImage from "./FeaturedNewsImage";
import {
    Row,
    Col
} from 'reactstrap';
// this will be replaced by a store 

class FeaturedNewsContent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const item = this.props.Item;
        return (
            <div className="__event __event--news">
                <Row>
                    <Col xs="5" md="4">
                        <FeaturedNewsImage Item={item} />
                    </Col>
                    <Col xs="7" md="8">
                        <FeaturedNewsLink Item={item} />
                        <FeaturedNewsDate Date={item} />
                    </Col>
                </Row>
            </div>
        );
    }
}


export default FeaturedNewsContent;

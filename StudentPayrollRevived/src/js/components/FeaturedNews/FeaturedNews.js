// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AddNewsItem, SetNewsItemActive, ClearNews } from "../../actions/featuredNewsActions";
import { Link as ALink } from 'react-router-dom';
import FeaturedNewsHeader from "./FeaturedNewsHeader";
import FeaturedNewsArr from "../../data/news";
import FeaturedNewsContent from "./FeaturedNewsContent";
import {
    Carousel,
    CarouselItem,
    CarouselIndicators
} from 'reactstrap';
import months from '../../data/months';
// this will be replaced by a store 

class FeaturedNews extends Component {
    constructor(props) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }
    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === this.props.news.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? this.props.news.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }
    componentWillMount() {
        this.props.actions.ClearNews();
        for (let item of FeaturedNewsArr) {
            this.props.actions.AddNewsItem(item);
        }
        this.props.actions.SetNewsItemActive(FeaturedNewsArr[0].Id);
    }

    render() {
        const { activeIndex } = this.state;
        const items = this.props.news;
        const slides = this.props.news.map((item) => (
            <CarouselItem
                onExiting={this.onExiting}
                onExited={this.onExited}
                key={item.Id}
            >
                <FeaturedNewsContent Item={item} />
            </CarouselItem>
        ));
        return (
            <div className="rsgis-featured">
                <FeaturedNewsHeader />
                <div className="__feature">
                    <Carousel keyboard={true} activeIndex={activeIndex} next={this.next} previous={this.previous}>
                        {slides}
                        <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                    </Carousel>
                </div>
            </div>

        );
    }
}



const mapStateToProps = (state) => {
    return {
        news: state.news.NewsArr,
        active: state.news.ActiveItem
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            AddNewsItem: (obj = {}) => { dispatch(AddNewsItem(obj)) },
            SetNewsItemActive: (id = 1) => { dispatch(SetNewsItemActive(id)) },
            ClearNews: () => { dispatch(ClearNews()) },
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(FeaturedNews);


{/* <div className="rsgis-featured">
                <div className="__header">
                    <div className="__icon __icon--news"></div>
                    <div className="__gray">Featured&nbsp;</div>
                    <div className="__green">News</div>
                </div>
                <div className="__feature">
                    <div id="featurednews-caro" className="carousel slide" data-ride="carousel" data-interval="false">
                        <div className="carousel-inner">
                            {this.RenderNewsItems()}
                        </div>
                        <ol className="carousel-indicators">
                            {this.RenderIndicators()}
                        </ol>
                    </div>
                </div>
            </div> */}
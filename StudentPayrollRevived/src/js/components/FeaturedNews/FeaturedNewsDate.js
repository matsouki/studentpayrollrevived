// React---------------------------------------
import React, { Component } from 'react';
import months from '../../data/months';
// this will be replaced by a store 

class FeaturedNewsDate extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    GetDate(d = new Date()) {
        return `${months[d.getMonth()].toUpperCase().substring(0, 3)} ${(d.getDate()).toString().length === 1 ? `0${d.getDate()}` : d.getDate()}, ${d.getFullYear()}`
    }
    render() {
        const {Date} = this.props.Date;
        return (<h3 className="__date">{this.GetDate(Date)}</h3>);
    }
}


export default FeaturedNewsDate;

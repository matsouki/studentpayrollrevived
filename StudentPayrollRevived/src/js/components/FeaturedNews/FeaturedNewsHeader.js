// React---------------------------------------
import React from 'react';

export default () => (
    <div className="__header">
        <div className="__icon __icon--news"></div>
        <div className="__gray">Featured&nbsp;</div>
        <div className="__green">News</div>
    </div>
);

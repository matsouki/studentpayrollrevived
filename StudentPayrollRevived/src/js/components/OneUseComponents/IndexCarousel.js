// React---------------------------------------
import React, { Component } from 'react';
import Image from 'react-image-webp';
// React Carousel------------------------------
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
// Other---------------------------------------
import baseUrl from "../../baseUrl";


const carouselPiecesDefault = [
    {
        id:0,
        photo: `${baseUrl.path}/images/caro1.jpg`,
        webp: `${baseUrl.path}/images/caro1.webp`,
        headText: 'RSGIS',
        subText:'Remote Sensing and Geographic Information Systems '
    },
    {
        id:1,
        photo: `${baseUrl.path}/images/caro2.jpg`,
        webp: `${baseUrl.path}/images/caro2.webp`,
        headText: 'RSGIS',
        subText:'Remote Sensing and Geographic Information Systems '
    }
];

export default class IndexCarousel extends Component {
    constructor(props){
        super(props); 

        this.state = {
            carouselPieces: carouselPiecesDefault
        };
    }

    render() {
        return (
            <Carousel autoPlay infiniteLoop={true} showIndicators={false} showThumbs={false} interval={10000}>
                {this.state.carouselPieces && this.state.carouselPieces.length > 0 ?
                    this.state.carouselPieces.map((carouselPiece, index) => {
                        return (
                            <div key={index} className="mainCarousel">
                                <picture>
                                    <source srcSet={carouselPiece.webp} type="image/webp"  alt={`RSGIS carousel picture ${index+1}`}/>
                                    <source srcSet={carouselPiece.photo} type="image/jpeg"  alt={`RSGIS carousel picture ${index+1}`}/>
                                    <img src={carouselPiece.photo} alt={`RSGIS carousel picture ${index+1}`}/>
                                </picture>
                                <p className="legend caro-legend">
                                    <b>{carouselPiece.headText}</b>
                                    <br/>
                                    {carouselPiece.subText}
                                </p>
                            </div>
                        );
                    })
                :
                    ""
                }
            </Carousel>
        );
    }
}; 
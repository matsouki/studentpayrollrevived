// React---------------------------------------
import React from 'react';

export default () => (
    <div className="__header">
        <div className="__icon __icon--event"></div>
        <div className="__gray">Featured&nbsp;</div>
        <div className="__green">Events</div>
    </div>
);

// React---------------------------------------
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AddEventsItem, SetEventsItemActive, ClearEvents } from "../../actions/featuredEventsActions";
import FeaturedEventsHeader from "./FeaturedEventsHeader";
import FeaturedEventsArr from "../../data/events";
import FeaturedEventContent from "./FeaturedEventContent";
import {
    Carousel,
    CarouselItem,
    CarouselIndicators
} from 'reactstrap';
// this will be replaced by a store 

class FeaturedEvents extends Component {
    constructor(props) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }
    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === this.props.items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? this.props.items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }
    componentWillMount() {
        this.props.actions.ClearEvents();
        for (let item of FeaturedEventsArr) {
            this.props.actions.AddEventsItem(item);
        }
        this.props.actions.SetEventsItemActive(FeaturedEventsArr[0].Id);
    }
    RenderSlides() {
        return this.props.items.map((item) => {
            return (
                <CarouselItem onExiting={this.onExiting} onExited={this.onExited} key={item.Id} >
                    <FeaturedEventContent Item={item} />
                </CarouselItem>
            );
        });
    }
    render() {
        const { activeIndex } = this.state;
        const items = this.props.items;
        return (
            <div className="rsgis-featured">
                <FeaturedEventsHeader />
                <div className="__feature">
                    <Carousel keyboard={true} activeIndex={activeIndex} next={this.next} previous={this.previous}>
                        {this.RenderSlides()}
                        <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                    </Carousel>
                </div>
            </div>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        items: state.events.EventsArr,
        active: state.events.ActiveItem
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            AddEventsItem: (obj = {}) => { dispatch(AddEventsItem(obj)) },
            SetEventsItemActive: (id = 1) => { dispatch(SetEventsItemActive(id)) },
            ClearEvents: () => { dispatch(ClearEvents()) },
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(FeaturedEvents);

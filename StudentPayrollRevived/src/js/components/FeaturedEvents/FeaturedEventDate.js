// React---------------------------------------
import React, { Component } from 'react';
import months from '../../data/months';
// this will be replaced by a store 

class FeaturedEventDate extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const item = this.props.Date;
        return (
            <div className="__date">
                <div className="__item">{months[item.Date.getMonth()].toUpperCase().substring(0, 3)}</div>
                <div className="__item">{(item.Date.getDate()).toString().length === 1 ? `0${item.Date.getDate()}` : item.Date.getDate()}</div>
                <div className="__item">{item.Date.getFullYear()}</div>
            </div>
        );
    }
}


export default FeaturedEventDate;

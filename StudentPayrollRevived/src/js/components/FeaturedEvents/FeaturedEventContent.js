// React---------------------------------------
import React, { Component } from 'react';
import FeaturedEventDate from "./FeaturedEventDate";
import FeaturedEventLink from "./FeaturedEventLink";
import {
    Row,
    Col
} from 'reactstrap';
// this will be replaced by a store 

class FeaturedEventContent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const item = this.props.Item;
        return (
            <div className="__event __event--event">
                <Row>
                    <Col xs="5" md="4">
                        <FeaturedEventDate Date={item} />
                    </Col>
                    <Col xs="7" md="8">
                        <FeaturedEventLink Item={item} />
                    </Col>
                </Row>
            </div>
        );
    }
}


export default FeaturedEventContent;

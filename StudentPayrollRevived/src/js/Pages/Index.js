// React---------------------------------------
import React, { Component } from 'react';
import Loadable from "react-loadable";
import { connect } from 'react-redux';
// Actions-------------------------------------
import { setLayoutType } from '../actions/layoutActions';
// Loadable Components-------------------------
const LoadableIndexCarousel = Loadable({
    loader: () => import("../components/OneUseComponents/IndexCarousel"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
const LoadableFeaturedNews = Loadable({
    loader: () => import("../components/FeaturedNews/FeaturedNews"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});
const LoadableFeaturedEvents = Loadable({
    loader: () => import("../components/FeaturedEvents/FeaturedEvents"),
    loading() {
        return <div><i className="fas fa-spinner fa-pulse"></i> Loading...</div>
    }
});


class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    componentDidMount() {
        if (this.props.type !== "transparent") {
            this.props.actions.setLayoutType()
        }
    }

    render(){
        return(
            <div className="wrapper __wrapper">
                {/* Caro  */}
                <LoadableIndexCarousel />  
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-md-6 col-lg-6">
                            <LoadableFeaturedNews />
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-6">
                            <LoadableFeaturedEvents />    
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "transparent") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Index);
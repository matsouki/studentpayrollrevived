// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import Image from 'react-image-webp';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'


class HallOfFame extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }
    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}/images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}/images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">Hall of Fame</h1>
                            <h3>William R. Enslin</h3>
                            <div className="about">    
                                <p>On October 17, 2006, we lost a valued friend and colleague.</p>    
                                <p>Bill did much to advance geospatial science in Michigan. Among his many accomplishments, Bill can be credited with bringing GIS software to local government through C-Map, Landscan, and the Michigan MapImage Viewer. Additionally, Bill was the co-founder of IMAGIN (Improving Michigan's Access to Geographic Information Networks), served on the board of LIAA (Land Information Access Association), and was a recipient of many prestigious awards.</p>
                                <p>Bill was a dedicated employee of RS&GIS for over 30 years.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(HallOfFame);
// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import Image from 'react-image-webp';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'


class History extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }
    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}/images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}/images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">History</h1>
                            <div className="about">        
                                <p>Founded in 1972 as the Remote Sensing Project, Michigan State University was among the first universities to experiment with the use of remote sensing technology for issues related to land use. For more than a decade, annual grants from NASA supported activities to advance applications of remote sensing in Michigan by means of collaborative research, technology transfer, and training programs in four core areas: Applications of Low-Altitude Aerial Photography, Analysis of Imagery from Satellite Sensors, Land Cover and Forestry Inventories, and Geographic Information Systems.</p>
                                <p>To be continued...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(History);
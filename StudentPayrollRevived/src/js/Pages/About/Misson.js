// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import Image from 'react-image-webp';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'


class Mission extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }
    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}/images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}/images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">Our Mission</h1>
                            <div className="about">        
                                <p>RS&GIS is a team of highly skilled professionals dedicated to providing innovative, high quality geospatial technology services that exceed the expectations of our clients.</p>
                                <ul>
                                    <li>
                                        <p>Work:  We provide superior services through a personalized and cost-effective approach.  We strive for excellence in the fields of Remote Sensing, Geographic Information Science (GIS), Global Positioning Systems (GPS), application development, and web/graphic design</p>
                                    </li>
                                    <li>
                                        <p>Team:  Together, the breadth and depth of our understanding is unsurpassed.  We strive for continual growth - personally, professionally, and organizationally.</p>
                                    </li>
                                    <li>
                                        <p>Clients:  Our business relationships are personal.  Founded in an environment of trust, respect, and communication, we strive to make every new customer a returning customer.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Mission);
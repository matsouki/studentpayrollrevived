// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import Image from 'react-image-webp';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
import staffData from "../../data/staff";
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'


class Staff extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }

    sortArrayByOrder(arrayToSort){
        let array = [ ...arrayToSort ];
        for(let i = 0; i < array.length; i++) {
            let temp = array[i];
            let j = i - 1;
            while (j >= 0 && array[j].order > temp.order) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = temp;
        }
        return array;
    }

    renderPostions(staff){
        let posArray = []

        if(staff){

            if(staff.positions && staff.positions.length > 0){
                staff.positions.map((position, index) => {
                    posArray.push(
                        <p key={index} className="no-margin-bottom">{position}</p>
                    );
                });
            }
        }

        return posArray;
    }

    renderNameWithDegreeCerts(staff){
        const name = staff.name
        let degrees = ""
        let certs = ""

        if(staff){

            if(staff.degrees && staff.degrees.length > 0){
                staff.degrees.map((degree) => {
                    degrees = degrees + `, ${degree}`
                });
            }

            if(staff.certifications && staff.certifications.length > 0){
                staff.certifications.map((cert) => {
                    certs = certs + `, ${cert}`
                });
            }
        }

        return (name + degrees + certs);
    }

    renderCards(){
        // console.log(staffData)
        let staffCardArray = []
        if(staffData && staffData.length > 0){
            const ourStaff = this.sortArrayByOrder(staffData)

            ourStaff.map((staff) => {
                staffCardArray.push(
                    <div key={staff.order} className="dashboard-card">
                        <header 
                            // onClick={() => {this.toggleAccordion()}}
                            className="dashboard-card-content flex-card msugreen-card"
                        >
                            <p className="spacing-fix white-letters">{this.renderNameWithDegreeCerts(staff)}</p>
                        </header>
                        <div className={`dashboard-card-content`}>
                            {this.renderPostions(staff)}
                            <p className="no-margin-bottom">{staff.room}</p>
                            <p className="no-margin-bottom">{staff.phone}</p>
                            <p className="no-margin-bottom"><a href={`mailto:${staff.email}`}>{staff.email}</a></p>
                        </div>
                    </div>
                );
            });
        }
        let half = Math.round(parseFloat(staffCardArray.length / 2));
        // console.log(half);
        let fristCol = staffCardArray.splice(0, half);



        return (
            <Row>
                <Col xs="12" sm="12" md="6" lg="6">
                    {fristCol}
                </Col>
                <Col xs="12" sm="12" md="6" lg="6">
                    {staffCardArray}
                </Col>
            </Row>
        );
    }

    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}/images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}/images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">Staff</h1>
                            <div className="about">        
                                <p>RS&GIS employs {staffData ? staffData.length : 0 } full time, professional staff. These experienced personnel conduct research, teach, and provide services in a variety of geography related fields. In addition, RS&GIS employs several full time and part-time technical staff to assist with these endeavors.</p>
                                <p>RS&GIS is proud to have several staff members on the registry of Certified GIS Professionals. The stringent certification process requires years of experience in the field, continued professional development, and service to the GIS community. The GIS Certification Institute has awarded this GISP designation to the following individuals: Robert Goodwin, Nick Weil, Joe Welsh and Dave Lusch.</p>
                            </div>
                            {this.renderCards()}
                        </div>
                    </div>
                </div> 
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Staff);
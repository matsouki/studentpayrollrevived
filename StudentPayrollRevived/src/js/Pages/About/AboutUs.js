// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import Image from 'react-image-webp';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'

class AboutUs extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }
    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">About Us</h1>
                            <div className="about">        
                                <p>RS&GIS is the point of contact for geospatial technology services at Michigan State University. Serving the research community, state, federal, municipal, and tribal governments, not-for-profits, and the private sector, the RS&GIS professional staff boasts over a century of combined expertise in the areas of GIS, remote sensing (RS), global positioning systems (GPS), application development, cartography and graphic design. Housed at Michigan State University, RS&GIS is in a unique position to access the technical, innovative, and human resources of the institution, while maintaining a competitive and service oriented mission.</p>
                                
                                <h3>Location:</h3>
                                <p>
                                    Nisbet Building<br/>
                                    1407 South Harrison Road, Suite 301<br/>
                                    East Lansing, MI 48823
                                </p>

                                <h3>Geographic Coordinates:</h3>
                                <p>
                                    Latitude: 42.7157692<br/>
                                    Longitude: -84.4929779<br/>
                                    N 42° 42' 56.7691"<br/>
                                    W 84° 29' 34.7204"
                                </p>

                                <h3>Contact Info:</h3>
                                <p>
                                    Phone: (517) 432-0446<br/>
                                    Fax: (517) 353-1821<br/>
                                    E-mail: <a href="mailto:RSGIS.Info@campusad.msu.edu">RSGIS.Info</a>
                                </p>

                                {/* TODO: Possible Google Map? */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);
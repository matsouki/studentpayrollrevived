// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Image from 'react-image-webp';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'

class Services extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }
    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">Services</h1>
                            <div className="about">
                                <ul>
                                    <li><Link to={`${baseUrl.route}services/gis`}>Geographic Information Systems (GIS)</Link></li>
                                    <li><Link to={`${baseUrl.route}services/uas`}>Unmanned Aerial Systems (UAS)</Link></li>
                                    <li><Link to={`${baseUrl.route}services/imagery`}>Imagery</Link></li>
                                    <li><Link to={`${baseUrl.route}services/applicationdevelopment`}>Application Development</Link></li>
                                    <li><Link to={`${baseUrl.route}services/gps`}>Global Positioning Systems (GPS)</Link></li>
                                    <li><Link to={`${baseUrl.route}services/cartographygraphicdesign`}>Cartography & Graphic Design</Link></li>
                                    <li><Link to={`${baseUrl.route}services/webdesign`}>Web Design</Link></li>
                                    <li><Link to={`${baseUrl.route}services/training`}>Training</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Services);
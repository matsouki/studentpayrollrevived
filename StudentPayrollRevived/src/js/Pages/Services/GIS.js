// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Image from 'react-image-webp';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'

class GIS extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }
    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">Geographic Information Systems (GIS)</h1>
                            <div className="about">
                                <ul>
                                    <li>General consulting</li>
                                    <li>Needs assessment</li>
                                    <li>Planning and Implementation</li>
                                    <li>Data development (digitizing, parcel mapping, scanning, field collection, data mining, etc.)</li>
                                    <li>Data manipulation (coordinate conversion, resolution resampling, file format conversion, etc.)</li>
                                    <li>Attribute standardization</li>
                                    <li>Geodatabase schemas</li>
                                    <li>Data processing and automation</li>
                                    <li>Geoprocessing analysis</li>
                                    <li>Network analysis</li>
                                    <li>Geocoding/address matching</li>
                                    <li>Land cover/land use data development and updates</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(GIS);
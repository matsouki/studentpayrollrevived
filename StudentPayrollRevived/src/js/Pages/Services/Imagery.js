// React---------------------------------------
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Image from 'react-image-webp';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
// Other---------------------------------------
import baseUrl from '../../baseUrl'
// Actions-------------------------------------
import { setLayoutType } from '../../actions/layoutActions'

class Imagery extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        if (this.props.type !== "standard") {
            this.props.actions.setLayoutType()
        }
    }
    render(){
        return(
            <div className="wrapper __wrapper __wrapper--standard">
                <div id="TopPane" className="headerImage">
                    <picture>
                        <source srcSet={`${baseUrl.path}images/mainheader.webp`} type="image/webp"/>
                        <source srcSet={`${baseUrl.path}images/mainheader.jpg`} type="image/jpeg"/>
                        <img src={`${baseUrl.path}/images/mainheader.jpg`}/>
                    </picture>
                </div>
                <div className="container con-rel">
                    <div className="row">
                        <div id="ContentPane" className="content col-12">
                            <h1 className="text-center">Services</h1>
                            <div className="about">
                                <ul>
                                    <li><Link to={`${baseUrl.route}archive`}>Aerial Imagery Archive</Link></li>
                                    <li>Online image services</li>
                                    <li>Image manipulation (registration, rectification/orthorectification, color-balancing, mosaicking, etc.)</li>
                                    <li>Image Interpretation (analog and digital, single and multiband)</li>
                                    <li>LiDAR surface creation (slope, aspect, contours, etc.)</li>
                                    <li>Multispectral and hyperspectral image analysis</li>
                                    <li>Impervious surface assessment</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        type: state.layout.view,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            setLayoutType: (view = "standard") => { dispatch(setLayoutType(view)) },
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Imagery);
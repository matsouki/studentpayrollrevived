const baseUrl = {
    // Base url for routes. This is left as "/" for development. But looking at the url, 
    // whatever is in between the domain and the react routes - 
    // example http://rsgis-webqa.campusad.msu.edu/RSGIS/services 
    // we would want, route: "/RSGIS/"
    route: "/StudentPayrollRevived/",
    // Base url for things like images or possible other assests.
    // This is left as public in most cases.
    path:"/StudentPayrollRevived/Content/public/"
    // Base url for the api. We want the entire url of the api
    // Different for everyone in most cases if using a local api
    // Currently this site does not use an api
    // api:"http://api.rsgis.com/api"
};

export default baseUrl;

import { createStore, combineReducers } from 'redux';

import layoutReducer from "../reducers/layoutReducer";
import navbarReducer from "../reducers/navbarReducer";
import featuredNewsReducer from "../reducers/featuredNewsReducer";
import featuredEventsReducer from "../reducers/featuredEventsReducer";


const reducer = combineReducers({
    news: featuredNewsReducer,
    events: featuredEventsReducer,
    layout: layoutReducer,
    navbar: navbarReducer,
});

export default () => {
    const store = createStore(
        reducer, /* preloadedState, */
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );


    return store;
}
/********************
 *      Fonts       *
 ********************/
require("./fonts/PER_____.woff");
require("./fonts/PER_____.ttf");
require("./fonts/PERI____.woff");
require("./fonts/PERI____.ttf");
require("./fonts/PERB____.woff");
require("./fonts/PERB____.ttf");
require("./fonts/PERBI___.woff");
require("./fonts/PERBI___.ttf");
/********************
 *   Images  *
 ********************/

require("./images/masthead-helmet-white.svg");
require("./images/masthead-helmet-green.svg");
require("./images/msu-wordmark-green.svg");
require("./images/caro1.jpg");
require("./images/caro2.jpg");
require("./images/RSGIS_white.png");
require("./images/featurednews1.jpg");
require("./images/featurednews2.jpg");
require("./images/featurednews3.jpg");
require("./images/mainheader.jpg");

// require("./images/webp/caro1.webp");
// require("./images/webp/caro2.webp");
// require("./images/webp/featurednews1.webp");
// require("./images/webp/featurednews2.webp");
// require("./images/webp/featurednews3.webp");
// require("./images/webp/mainheader.webp");


/********************
 *   CSS Libraries  *
 ********************/
/******************
 *   CSS Custom   *
 ******************/
require("./less/main.less");
/********************
 *   JS Libraries   *
 ********************/


require("./js/scripts/app.js");


if (module.hot) {
  module.hot.accept();
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentPayrollRevived.Models;
namespace StudentPayrollRevived.Interfaces
{
    public interface IViewModel
    {
        SignedInUserModel SignedInUser { get; set; }
    }
}
